MODULE obctrc
   !!=================================================================================
   !!                       ***  MODULE  obctrc  ***
   !! Biogeochemical tracers:   Radiation of tracers on each open boundary
   !!t code taken from obctra and modified for bgc obc, by tessa sou, changes marked by !t 
   !!t bgc obc only set up for east and west obc
   !!=================================================================================
#if defined key_obc && defined key_top
   !!---------------------------------------------------------------------------------
   !!   'key_obc'      :                                      Open Boundary Conditions
   !!   'key_top'      :  passive tracers 
   !!---------------------------------------------------------------------------------
   !!   obc_trc        : call the subroutine for each open boundary
   !!   obc_trc_east   : radiation of the east open boundary tracers
   !!   obc_trc_west   : radiation of the west open boundary tracers
   !!   obc_trc_north  : radiation of the north open boundary tracers
   !!   obc_trc_south  : radiation of the south open boundary tracers
   !!----------------------------------------------------------------------------------
   !! * Modules used
   USE oce             ! ocean dynamics and tracers variables
   USE dom_oce         ! ocean space and time domain variables 
   USE phycst          ! physical constants
   USE obc_oce         ! ocean open boundary conditions !t rdpeob,lfbceast, ln_obc_east =obc_par
                       ! obc_oce includes rfoe* !t
   USE lib_mpp         ! ???
   USE lbclnk          ! ???
   USE in_out_manager  ! I/O manager		!t nit000, ln_rstart

   USE par_trc , ONLY : jptra ! number of passive tracers !t
   USE trc             ! passive tracers common variables !t incl for trn, tra

   IMPLICIT NONE
   PRIVATE

   !! * Accessibility
   PUBLIC obc_trc     ! routine called in trcnxt.F90 

   !! * Module variables
   INTEGER ::      & ! ... boundary space indices 
      nib   = 1,   & ! nib   = boundary point
      nibm  = 2,   & ! nibm  = 1st interior point
      nibm2 = 3,   & ! nibm2 = 2nd interior point
                     ! ... boundary time indices 
      nit   = 1,   & ! nit    = now
      nitm  = 2,   & ! nitm   = before
      nitm2 = 3      ! nitm2  = before-before

   REAL(wp) ::     &
      rtaue  , rtauw  , rtaun  , rtaus  ,  &  ! Boundary restoring coefficient
      rtauein, rtauwin, rtaunin, rtausin      ! Boundary restoring coefficient for inflow 

   !! * Substitutions
#  include "obc_vectopt_loop_substitute.h90"
   !!---------------------------------------------------------------------------------
   !! NEMO/OPA 3.3 , NEMO Consortium (2010)
   !! $Id: obctra.F90 2977 2011-10-22 13:46:41Z cetlod $ 
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!---------------------------------------------------------------------------------

CONTAINS

   SUBROUTINE obc_trc ( kt )
      !!-------------------------------------------------------------------------------
      !!                 ***  SUBROUTINE obc_trc  ***
      !!                    
      !! ** Purpose :   Compute tracer fields (t,s) along the open boundaries.
      !!      This routine is called by the tranxt.F routine and updates tsa
      !!      which are the actual temperature and salinity fields.
      !!        The logical variable lp_obc_east, and/or lp_obc_west, and/or lp_obc_north,
      !!      and/or lp_obc_south allow the user to determine which boundary is an
      !!      open one (must be done in the param_obc.h90 file).
      !!
      !! Reference : 
      !!   Marchesiello P., 1995, these de l'universite J. Fourier, Grenoble, France.
      !
      !!  History :
      !!        !  95-03 (J.-M. Molines) Original, SPEM
      !!        !  97-07 (G. Madec, J.-M. Molines) addition
      !!   8.5  !  02-10 (C. Talandier, A-M. Treguier) F90
      !!----------------------------------------------------------------------
      !! * Arguments
      INTEGER, INTENT( in ) ::   kt
      INTEGER ::   jn      ! dummy loop indices !t
      !!----------------------------------------------------------------------

      ! 0. Local constant initialization

      IF( kt == nit000 .OR. ln_rstart) THEN
         ! ... Boundary restoring coefficient
         rtaue = 2. * rdt / rdpeob   
         rtauw = 2. * rdt / rdpwob
         rtaun = 2. * rdt / rdpnob
         rtaus = 2. * rdt / rdpsob
         ! ... Boundary restoring coefficient for inflow ( all boundaries)
         rtauein = 2. * rdt / rdpein 
         rtauwin = 2. * rdt / rdpwin
         rtaunin = 2. * rdt / rdpnin
         rtausin = 2. * rdt / rdpsin 
      END IF

      IF( lp_obc_east  )   CALL obc_trc_east ( kt )    ! East open boundary

      IF( lp_obc_west  )   CALL obc_trc_west ( kt )    ! West open boundary 
 
      !tIF( lp_obc_north )   CALL obc_trc_north( kt )    ! North open boundary !t not done 

      !tIF( lp_obc_south )   CALL obc_trc_south( kt )    ! South open boundary !t not done

      IF( lk_mpp ) THEN                  !!bug ???
         IF( kt >= nit000+3 .AND. ln_rstart ) THEN
            DO jn = 1, jptra                       !t update tra
             CALL lbc_lnk( tra(:,:,:,jn), 'T', 1. )
            END DO
         END IF
           DO jn = 1, jptra                       !t update tra
            CALL lbc_lnk( tra(:,:,:,jn), 'T', 1. )
           END DO
      ENDIF

   END SUBROUTINE obc_trc


   SUBROUTINE obc_trc_east ( kt )
      !!------------------------------------------------------------------------------
      !!                ***  SUBROUTINE obc_trc_east  ***
      !!                  
      !! ** Purpose :
      !!      Apply the radiation algorithm on east OBC tracers tsa using the 
      !!      phase velocities calculated in obc_rad_east subroutine in obcrad.F90 module
      !!      If the logical lfbceast is .TRUE., there is no radiation but only fixed OBC
      !!
      !!  History :
      !!         ! 95-03 (J.-M. Molines) Original from SPEM
      !!         ! 97-07 (G. Madec, J.-M. Molines) additions
      !!         ! 97-12 (M. Imbard) Mpp adaptation
      !!         ! 00-06 (J.-M. Molines) 
      !!    8.5  ! 02-10 (C. Talandier, A-M. Treguier) F90
      !!------------------------------------------------------------------------------
      !! * Arguments
      INTEGER, INTENT( in ) ::   kt

      !! * Local declaration
      INTEGER ::   ji, jj, jk      ! dummy loop indices
      INTEGER ::   jn      ! dummy loop indices !t
      REAL(wp) ::   z05cx, ztau, zin
      !!------------------------------------------------------------------------------

      ! 1. First three time steps and more if lfbceast is .TRUE.
      !    In that case open boundary conditions are FIXED.
      ! --------------------------------------------------------
      IF( ( kt < nit000+3 .AND. .NOT.ln_rstart ) .OR. lfbceast ) THEN
         DO jn = 1, jptra ! passive tracers !t 
         DO ji = fs_nie0+1, fs_nie1+1 ! Vector opt.
            DO jk = 1, jpkm1
               DO jj = 1, jpj
!t                  tra(ji,jj,jk,1) = 2.312e-3_wp ! trcini_pisces.f90 sco2 DIC default initi value
                   tra(ji,jj,jk,jn) = tra(ji,jj,jk,jn) * (1. - temsk(jj,jk)) + &
                                      rfoe(jj,jk,jn)*temsk(jj,jk)  !t
               END DO
            END DO
         END DO
       END DO ! passive tracers !t

      ELSE

      ! 2. Beyond the fourth time step if lfbceast is .FALSE.
      ! -----------------------------------------------------

         ! Temperature and salinity radiation
         ! ----------------------------------
         !
         !            nibm2      nibm      nib
         !              |   nibm  |   nib///|///
         !              |    |    |    |////|///
         !  jj   line --v----f----v----f----v---
         !              |    |    |    |////|///
         !                   |         |///   //
         !  jj   line   T    u    T    u/// T //
         !                   |         |///   //
         !              |    |    |    |////|///
         !  jj-1 line --v----f----v----f----v---
         !              |    |    |    |////|///
         !                jpieob-1    jpieob / ///
         !              |         |         |
         !           jpieob-1    jpieob     jpieob+1
         !
         ! ... radiative conditions + relaxation toward a climatology
         !     the phase velocity is taken as the phase velocity of the tangen-
         !     tial velocity (here vn), which have been saved in (u_cxebnd,v_cxebnd)
         ! ... (jpjedp1, jpjefm1), jpieob+1
           DO jn = 1, jptra ! passive tracers !t
           DO ji = fs_nie0+1, fs_nie1+1 ! Vector opt.
            DO jk = 1, jpkm1
               DO jj = 2, jpjm1  !t why 2,jpjm1 instead of 1,jpj?
        ! ... i-phase speed ratio (from averaged of v_cxebnd)  !t does these linked in for obctrc?
                  z05cx = ( 0.5 * ( v_cxebnd(jj,jk) + v_cxebnd(jj-1,jk) ) ) / e1t(ji-1,jj)
                  z05cx = min( z05cx, 1. )
         ! ... z05cx=< 0, inflow  zin=0, ztau=1    
         !           > 0, outflow zin=1, ztau=rtaue
                  zin = sign( 1., z05cx )
                  zin = 0.5*( zin + abs(zin) )
         ! ... for inflow rtauein is used for relaxation coefficient else rtaue
                  ztau = (1.-zin ) * rtauein  + zin * rtaue
                  z05cx = z05cx * zin
         ! ... update tra with radiative or climatological 
                  tra(ji,jj,jk,jn) = tra(ji,jj,jk,jn) * (1. - temsk(jj,jk)) + &
                                 temsk(jj,jk) * ( ( 1. - z05cx - ztau )         &
                                 * rebnd(jj,jk,nib ,nitm,jn) + 2.*z05cx              &
                                 * rebnd(jj,jk,nibm,nit ,jn) + ztau * rfoe (jj,jk,jn) ) &
                                 / (1. + z05cx)
              END DO ! jj
            END DO ! jk
         END DO ! ji
         END DO !jn passive tracers !t

      END IF  ! lfbceast

   END SUBROUTINE obc_trc_east


   SUBROUTINE obc_trc_west ( kt )
      !!------------------------------------------------------------------------------
      !!                 ***  SUBROUTINE obc_trc_west  ***
      !!           
      !! ** Purpose :
      !!      Apply the radiation algorithm on west OBC tracers tsa using the 
      !!      phase velocities calculated in obc_rad_west subroutine in obcrad.F90 module
      !!      If the logical lfbcwest is .TRUE., there is no radiation but only fixed OBC
      !!
      !!  History :
      !!         ! 95-03 (J.-M. Molines) Original from SPEM
      !!         ! 97-07 (G. Madec, J.-M. Molines) additions
      !!         ! 97-12 (M. Imbard) Mpp adaptation
      !!         ! 00-06 (J.-M. Molines) 
      !!    8.5  ! 02-10 (C. Talandier, A-M. Treguier) F90
      !!------------------------------------------------------------------------------
      !! * Arguments
      INTEGER, INTENT( in ) ::   kt

      !! * Local declaration
      INTEGER ::   ji, jj, jk      ! dummy loop indices
      INTEGER ::   jn      ! dummy loop indices !t

      REAL(wp) ::   z05cx, ztau, zin
      !!------------------------------------------------------------------------------

      ! 1. First three time steps and more if lfbcwest is .TRUE.
      !    In that case open boundary conditions are FIXED.
      ! --------------------------------------------------------
      IF( ( kt < nit000+3 .AND. .NOT.ln_rstart ) .OR. lfbcwest ) THEN
        DO jn = 1, jptra ! passive tracers !t
         DO ji = fs_niw0, fs_niw1 ! Vector opt.
            DO jk = 1, jpkm1
               DO jj = 1, jpj
                  tra(ji,jj,jk,jn) = tra(ji,jj,jk,jn) * (1. - twmsk(jj,jk)) + &
                                         rfow(jj,jk,jn)*twmsk(jj,jk)
               END DO
            END DO
         END DO

       END DO ! passive tracers !t
      ELSE

      ! 2. Beyond the fourth time step if lfbcwest is .FALSE.
      ! -----------------------------------------------------
          
         ! Temperature and salinity radiation
         ! ----------------------------------
         !
         !          nib       nibm     nibm2
         !     nib///|   nibm  |  nibm2  |
         !   ///|////|    |    |    |    |   
         !   ---v----f----v----f----v----f-- jj   line
         !   ///|////|    |    |    |    |   
         !   //   ///|         |         |   
         !   // T ///u    T    u    T    u   jj   line
         !   //   ///|         |         |   
         !   ///|////|    |    |    |    |   
         !   ---v----f----v----f----v----f-- jj-1 line
         !   ///|////|    |    |    |    |   
         !         jpiwob    jpiwob+1    jpiwob+2
         !      |         |         |        
         !    jpiwob    jpiwob+1   jpiwob+2
         !
         ! ... radiative conditions + relaxation toward a climatology
         ! ... the phase velocity is taken as the phase velocity of the tangen-
         ! ... tial velocity (here vn), which have been saved in (v_cxwbnd)
         DO jn = 1, jptra ! passive tracers !t

         DO ji = fs_niw0, fs_niw1 ! Vector opt.
            DO jk = 1, jpkm1
               DO jj = 2, jpjm1
         ! ... i-phase speed ratio (from averaged of v_cxwbnd)
                  z05cx = (  0.5 * ( v_cxwbnd(jj,jk) + v_cxwbnd(jj-1,jk) ) ) / e1t(ji+1,jj)
                  z05cx = max( z05cx, -1. )
         ! ... z05cx > 0, inflow  zin=0, ztau=1    
         !           < 0, outflow zin=1, ztau=rtauw
                  zin = sign( 1., -1.* z05cx )
                  zin = 0.5*( zin + abs(zin) )
                  ztau = (1.-zin )*rtauwin + zin * rtauw
                  z05cx = z05cx * zin
         ! ... update tra with radiative or climatological
         !t jan31/14 note the sign difference of z05cx compared to east bnd
                  tra(ji,jj,jk,jn) = tra(ji,jj,jk,jn) * (1. - twmsk(jj,jk)) + &
                                 twmsk(jj,jk) * ( ( 1. + z05cx - ztau )         &
                                 * rwbnd(jj,jk,nib ,nitm,jn) - 2.*z05cx              &
                                 * rwbnd(jj,jk,nibm,nit,jn) + ztau * rfow (jj,jk,jn) ) &
                                 / (1. - z05cx)

               END DO
            END DO
         END DO
         END DO !jn passive tracers !t

      END IF

   END SUBROUTINE obc_trc_west


#else
   !!---------------------------------------------------------------------------------
   !!   Default option                                                    Empty module
   !!---------------------------------------------------------------------------------
CONTAINS
   SUBROUTINE obc_trc      ! Empty routine
   END SUBROUTINE obc_trc
#endif

   !!=================================================================================
END MODULE obctrc
