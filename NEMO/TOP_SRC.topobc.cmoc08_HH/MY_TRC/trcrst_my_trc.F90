MODULE trcrst_my_trc
   !!======================================================================
   !!                       ***  MODULE trcrst_my_trc  ***
   !! TOP :   create, write, read the restart files of MY_TRC tracer
   !!======================================================================
   !! History :   1.0  !  2010-01 (C. Ethe)  Original
   !!----------------------------------------------------------------------
#if defined key_my_trc
   !!----------------------------------------------------------------------
   !!   'key_my_trc'                                               CFC tracers
   !!----------------------------------------------------------------------
   !!   trc_rst_read_my_trc   : read  restart file
   !!   trc_rst_wri_my_trc    : write restart file
   !!----------------------------------------------------------------------
!HH0
   USE iom
   USE trcsms_my_trc
!HH1

   IMPLICIT NONE
   PRIVATE

   PUBLIC  trc_rst_read_my_trc   ! called by trcini.F90 module
   PUBLIC  trc_rst_wri_my_trc   ! called by trcini.F90 module

CONTAINS
   
   SUBROUTINE trc_rst_read_my_trc( knum ) 
     INTEGER, INTENT(in)  :: knum
!HH0
#ifdef key_my_trc_iceeco
     IF(lwp) WRITE(numout,*) ' trc_rst_read_my_trc : Read specific variables from my_trc model'
     CALL iom_get( knum, jpdom_autoglo, 'icedia', icedia(:,:))
     CALL iom_get( knum, jpdom_autoglo, 'iceno3', iceno3(:,:))
     CALL iom_get( knum, jpdom_autoglo, 'icenh4', icenh4(:,:))
#ifdef key_my_trc_icedms
     CALL iom_get( knum, jpdom_autoglo, 'icedmspd', icedmspd(:,:))
     CALL iom_get( knum, jpdom_autoglo, 'icedms', icedms(:,:))
#endif
#endif
!    WRITE(*,*) 'trc_rst_read_my_trc: No specific variables to read on unit', knum
!HH1
   END SUBROUTINE trc_rst_read_my_trc

   SUBROUTINE trc_rst_wri_my_trc( kt, kitrst, knum )
     INTEGER, INTENT(in)  :: kt, kitrst, knum
!HH0
#ifdef key_my_trc_iceeco
     IF(lwp) WRITE(numout,*) ' trc_rst_write_my_trc : Write specific variables from my_trc model'
     CALL iom_rstput( kt, kitrst, knum, 'icedia', icedia(:,:))
     CALL iom_rstput( kt, kitrst, knum, 'iceno3', iceno3(:,:))
     CALL iom_rstput( kt, kitrst, knum, 'icenh4', icenh4(:,:))
#ifdef key_my_trc_icedms
     CALL iom_rstput( kt, kitrst, knum, 'icedmspd', icedmspd(:,:))
     CALL iom_rstput( kt, kitrst, knum, 'icedms', icedms(:,:))
#endif
#endif
!    WRITE(*,*) 'trc_rst_wri_my_trc: No specific variables to write on unit' ,knum, ' at time ', kt, kirst
!HH1
   END SUBROUTINE trc_rst_wri_my_trc

#else
   !!----------------------------------------------------------------------
   !!  Dummy module :                                     No passive tracer
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE trc_rst_read_my_trc( knum )
     INTEGER, INTENT(in)  :: knum
     WRITE(*,*) 'trc_rst_read_my_trc: You should not have seen this print! error?', knum
   END SUBROUTINE trc_rst_read_my_trc

   SUBROUTINE trc_rst_wri_my_trc( kt, kirst, knum )
     INTEGER, INTENT(in)  :: kt, kirst, knum
     WRITE(*,*) 'trc_rst_wri_my_trc: You should not have seen this print! error?', kt, kirst, knum
   END SUBROUTINE trc_rst_wri_my_trc
#endif

   !!======================================================================
END MODULE trcrst_my_trc
