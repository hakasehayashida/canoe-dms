!!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!! MY_TRC  :    1  - parameters for ocean dms module          (namdmsoce)
!! namelists    2  - parameters for ice ecosystem module      (namiceeco)
!!              3  - parameters for ice dms module            (namicedms)
!!              4  - additional 2D/3D  diagnostics            (nampisdia)
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&namdmsoce     !   Ocean DMS module
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
! q_p1,q_p2: intracellular DMSPp-to-Chl ratio for p1,p2 [nmolS:mgChl]
! f_z1,f_z2: sloppy feeding fraction for z1,z2 [-]
! f_e1,f_e2: exudation fraction for p1,p2 [-]
! f_yield: DMS yield [-]
! k_dmspd,k_dms: bacterial DMSPd,DMS consumption rate constant [d-1]
! k_free,k_photo: DMSPd,photolysis rate constant [d-1]
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   q_p1    = 50.
   q_p2    = 9.5
   f_z1    = 0.3
   f_z2    = 0.3
   f_e1    = 0.05
   f_e2    = 0.05
   f_yield = 0.2
   k_dmspd = 5.
   k_dms   = 0.5
   k_free  = 0.02
   k_photo = 0.1
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&namiceeco     !   Ice ecosystem module
!
! f_p2: seeding fraction [-]
! f_gz: grazing fraction [-]
! z_ia: skeletal layer thickness [m]
! c_ia: light attenuation by ice algae [mg chla m-2]
! r_ni: nitrification rate constant [d-1]
! b_ia: background ice algae concentration [mmol C m-3]
! h_ni:
! r_gr: ice algal growth rate constant [d-1]
! r_m1: ice algal linear mortality rate constat [d-1]
! r_m2: ice algal quadratic mortality rate constat [d-1]
! c_me: critical ice growth/melt rate [m d-1]
! r_pp: ratio of photosynthetic parameters [(W m-2)-1]
! t_ia: ice algal temperature sensitivity coefficient [degC-1]
! c_nu:
! c_di:
! c2n: ice algal intracellular carbon-to-nitrogen ratio [mol:mol]
! c2ch: ice algal intracellular carbon-to-chlorophyll ratio [mol:g]
! icedic: dic concentration in ice
! icetak: tak concentration in ice
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   f_p2 = 0.1
   f_gz = 0.0
   z_ia = 0.03 
   c_ia = 0.06
   r_ni = 0.01 
   b_ia = 10.0
   h_ni = 1.0 
   r_gr = 0.85 
   r_m1 = 0.03 
   r_m2 = 0.00015 
   c_me = 0.015 
   r_pp = 2.0
   t_ia = 0.0633
   c_nu = 1.85e-6 
   c_di = 0.47e-9 
   c2n  = 8.83
   c2ch = 2.33
   icedic = 400.0
   icetak = 500.0
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&namicedms     !   Ice DMS module
!
! q_pi: intracellular dmsp-to-chlorophyll ratio [nmol:ug]
! f_zi: sloppy feeding fraction [-]
! f_ei: exudation fraction [-]
! f_yieldi: bacterial conversion fraction [-]
! k_dmspdi: bacterial dmspd consumption rate constant [d-1]
! k_dmsi: bacterial dms consumption rate constat [d-1]
! k_freei: free lyase rate constat [d-1]
! k_photoi: photlysis rate constat [d-1]
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   q_pi     = 9.5
   f_zi     = 0.3
   f_ei     = 0.05
   f_yieldi = 0.2
   k_dmspdi = 1.
   k_dmsi   = 0.2
   k_freei  = 0.02
   k_photoi = 0.1
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nammytdia     !   additional 2D/3D tracers diagnostics
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
!              !    name   !    title of the field    !     units      !
!              !           !                          !                !
   mytdia2d(1)  = 'dmsflux'  , 'DMS flux            ', 'umol S m-2 s-1'
   mytdia2d(2)  = 'icedia'   , 'bottom ice diatom   ', 'mmol C m-3'
   mytdia2d(3)  = 'icechl'   , 'bottom ice chl-a    ', 'mg CHL m-3'
   mytdia2d(4)  = 'icefgrow' , 'ice algal growth rate', 'mmol C m-3 s-1'
   mytdia2d(5)  = 'iceno3'   , 'bottom ice nitrate  ', 'mmol N m-3'
   mytdia2d(6)  = 'icenh4'   , 'bottom ice ammonium ', 'mmol N m-3'
   mytdia2d(7)  = 'ice_lim_lig', 'IA light limitation', '-'
   mytdia2d(8)  = 'ice_lim_ice', 'IA ice limitation', '-'
   mytdia2d(9)  = 'ice_lim_nut', 'IA nutrient limitation', '-'
   mytdia2d(10)  = 'icedmspd' , 'bottom ice DMSPd    ', 'umol S m-3'
   mytdia2d(11)  = 'icedms'   , 'bottom ice DMS      ', 'umol S m-3'
/
