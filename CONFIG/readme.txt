---legacy---
cmoc06.7: version 1 of CANOE+DMS
cmoc08: version 2 of CANOE+DMS (updated parameters).
---latest version(s)---
cmoc08_naa1: NAA1 and produces the model output for the upper 100 m only (key_naa1).
cmoc08_naa6: NAA6 and produces the model output for the upper 100 m only (key_naa6).
