#!/bin/bash
#SBATCH --ntasks=1               # number of MPI processes
#SBATCH --mem-per-cpu=4000M      # memory; default unit is megabytes
#SBATCH --time=0-03:59           # time (DD-HH:MM)
#SBATCH --account=rrg-steinern
#SBATCH --mail-user=ericthemort@gmail.com
#SBATCH --mail-type=ALL

year=2010
cpus=240
freoutsuf=730h #watchout! this was set to 5d
freoutsuf2=1d  # this is the add on to be able to sort biolog as well  

for i in biolog ptrc_T diad_T icemod grid_T grid_U grid_V grid_W; do
	if [ "$i" = "biolog" ]; then
		fname=NAA_${freoutsuf2}_${year}0101_${year}1231_${i}
	else
		fname=NAA_${freoutsuf}_${year}0101_${year}1231_${i}
	fi
	$HOME/scratch/canoe-dms/TOOLS/REBUILD_NEMO/rebuild_nemo $fname ${cpus}
	if [ $? -eq 0 ]; then
        	ncks -4 -L 1 -O $fname.nc $fname.nc
		if [ $? -eq 0 ]; then
			mkdir -p ${dir_data}
			mv $fname.nc output
			if [ $? -eq 0 ]; then		
				rm ${fname}_0*.nc
			else
				echo "failed: mv $fname.nc"
				exit 1
			fi
		else
			echo "failed: if2, $i"
			exit 1
		fi
	else
		echo "failed: if1, $i"
		exit 1
	fi
done
