#Define the path and suffix to the restart files:
# dir_data=$HOME/data/cmoc08/EXP44/1979
dir_data=$HOME/scratch/canoe-dms/CONFIG/eric_cmoc_naa1/EXP300/output/1969

suf_rst=NAA_00026280

# link restart files
for i in restart restart_ice restart_trc;
do
 ln -sf ${dir_data}/${suf_rst}_${i}.nc ${i}_in.nc
done
ln -sf ${dir_data}/restart.obc.output restart.obc
