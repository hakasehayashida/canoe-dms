#!/bin/bash

echo "Enter the year of the first-year run: "
read yr0
echo "Enter the year of the final-year run: "
read yr1
echo "Type yes if the first-year run is a restart: "
echo "Or type no if the first-year run is not a restart: "
read rst
echo "Did you check mpi_job.pbs?"
echo "Did you check namelist*?"
echo "Did you check symbolic links to restart files?"

for iii in $(seq ${yr0} ${yr1})
do
	vi qsub_job.pbs -c ":%s/\(year=\).*/\1${iii}/g" -c ":wq"
	vi qsub_job.pbs -c ":%s/\(rst=\).*/\1${rst}/g" -c ":wq"
	vi sort_output.pbs -c ":%s/\(year=\).*/\1${iii}/g" -c ":wq"
	if [ ${iii} == ${yr0} ]; then # Initial year
		job=$(qsub -N ${iii} qsub_job.pbs)
		echo $job
		rst=yes
	else # Following years
		job=$(qsub -W depend=afterok:${job##* } -N ${iii} qsub_job.pbs)
		echo $job
	fi
	job2=$(qsub -W depend=afterok:${job##* } -N sort${iii} sort_output.pbs)
	echo $job2
done
