#!/bin/bash
#SBATCH --nodes=5
#SBATCH --tasks-per-node=48	#32 procs per node
#SBATCH --mem=0		#128GB per node
#SBATCH --time=0-05:30          # time (DD-HH:MM)
#SBATCH --account=rrg-steinern
#SBATCH --mail-user=ericthemort@gmail.com
#SBATCH --mail-type=ALL

year=2010
rst=yes
cpus=240
rst_suf=NAA_00026280

vi namelist -c ":%s/\(.*\)[0-9][0-9][0-9][0-9]\(0101.*\)/\1${year}\2/g" -c ":wq"
if [ "${rst}" == "yes" ]; then
	vi namelist -c ":%s/\(.*ln_rstart.*=\s\)\..*\.\(.*\)/\1.true.\2/g" -c ":wq"
	vi namelist_top -c ":%s/\(.*ln_rsttr.*=\s\)\..*\.\(.*\)/\1.true.\2/g" -c ":wq"
elif [ "${rst}" == "no" ]; then
	vi namelist -c ":%s/\(.*ln_rstart.*=\s\)\..*\.\(.*\)/\1.false.\2/g" -c ":wq"
	vi namelist_top -c ":%s/\(.*ln_rsttr.*=\s\)\..*\.\(.*\)/\1.false.\2/g" -c ":wq"
fi
ln -sf NAA_runoff_y${year}m00_DaiTrenberth.nc runoff_1m_nomask.nc
export MALLOC_CHECK_=1
mpirun ./opa               # mpirun or mpiexec also work

if [ $? -eq 0 ]; then
	mkdir -p output/${year}
	cp ocean.output tracer.stat output/${year}
	for i in restart restart_ice restart_trc;
	do
		$HOME/scratch/canoe-dms/TOOLS/REBUILD_NEMO/rebuild_nemo ${rst_suf}_${i} ${cpus}
		if [ $? -eq 0 ]; then
			ncks -4 -L 1 -O ${rst_suf}_${i}.nc ${rst_suf}_${i}.nc
			if [ $? -eq 0 ]; then
				mv ${rst_suf}_${i}.nc output/${year}
				ln -sf output/${year}/${rst_suf}_${i}.nc ${i}_in.nc
				if [ $? -eq 0 ]; then
					rm ${rst_suf}_${i}_0*.nc
				else
					echo "failed: mv and ln of ${rst_suf}_${i}"
					exit 1
				fi
			else
				echo "failed: ncks of ${rst_suf}_${i}"
				exit 1
			fi
		else
			echo "failed: rebuild_nemo of ${rst_suf}_${i}"
			exit 1
		fi
	done
	if [ $? -eq 0 ]; then
		mv restart.obc.output output/${year}
		ln -sf output/${year}/restart.obc.output restart.obc
	else
		echo "failed: for-loop"
		exit 1
	fi
else
	echo "failed: ./opa"
	exit 1
fi
