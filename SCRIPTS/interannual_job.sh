#!/bin/bash

echo "Enter the year of the first-year run: "
read yr0
echo "Enter the year of the final-year run: "
read yr1
echo "Type yes if the first-year run is a restart: "
echo "Or type no if the first-year run is not a restart: "
read rst
echo "Did you check mpi_job.sh?"
echo "Did you check namelist*?"
echo "Did you check symbolic links to restart files?"

for iii in $(seq ${yr0} ${yr1})
do
	vi mpi_job.sh -c ":%s/\(year=\).*/\1${iii}/g" -c ":wq"
	vi mpi_job.sh -c ":%s/\(rst=\).*/\1${rst}/g" -c ":wq"
	vi sort_output.sh -c ":%s/\(year=\).*/\1${iii}/g" -c ":wq"
	if [ ${iii} == ${yr0} ]; then # Initial year
		job=$(sbatch --job-name=${iii} mpi_job.sh)
		echo $job
		rst=yes
	else # Following years
		echo $job
		job=$(sbatch --dependency=afterok:${job##* } --job-name=${iii} mpi_job.sh)
		echo $job
	fi
	job2=$(sbatch --dependency=afterok:${job##* } --job-name=sort${iii} sort_output.sh)
	echo $job2
done
