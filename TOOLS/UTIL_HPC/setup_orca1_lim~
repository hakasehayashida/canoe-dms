########################################################################
##
## Script to run a NEMO experiment on HPC_IOS
##
## T.Wakamatsu (tsuyoshi.wakamatsu@dfo-mpo.gc.ca)
##
## Note: this script file must be placed under a directory 
##       with the following structure:
##
##         /CONFIG/"CONFIG_NAME"/"CASE_NAME"
##
##       e.g.) /CONFIG/ORCA2_LIM/EXE00
##
##       then all outputs files are stored under
##
##         $IO_HOME/ORCA2_LIM/EXE00
##
########################################################################
##-- print echo of commands before and after shell interpretation
date
set -vx

#################################
# Beginning of user modification
#################################

IO_HOME=/export/scratch2/wakamatsut/nemo_output/nemo_v3_3
R_CONFIG=/export/scratch2/wakamatsut/nemo_input/ORCA1_NOCS/INPUTS
R_CONFIGZ=/export/scratch2/wakamatsut/nemo_input/ORCA1_NOCS/Z46
R_FORCING=/export/scratch2/wakamatsut/nemo_input/CORE_v2/CNYF_2.0
R_FORCINGW=/export/scratch2/wakamatsut/nemo_input/ORCA1_NOCS/WEIGHTS

#################################
# End of user modification
#################################

EXPNAM=$(basename $PWD)                 
CNFNAM=$(basename $(dirname $PWD))      

##-- define some directories:

#- modipsl repository
R_EXEDIR=$(dirname $(dirname $(dirname $PWD)))
#R_EXEDIR=$(dirname $PWD)

#- utility repositories
R_IOIPSL_TOOL=

#- execution repository
R_TMP=${IO_HOME}/${CNFNAM}/${EXPNAM}
[ -d ${R_TMP} ] || mkdir -p ${R_TMP}

#- 
R_EXPER=$PWD

##-- move to the execution repository

cd ${R_TMP}

#- copy the executable
ln -s ${R_EXEDIR}/CONFIG/${CNFNAM}/BLD/bin/nemo.exe opa.xx
chmod 777 opa.xx

##-- copy input files

#- Namelist for ocean and ice
ln -fs  ${R_EXPER}/namelist     ./namelist
ln -fs  ${R_EXPER}/namelist_ice ./namelist_ice

#- Namelist for ocean and ice (agrif fine grid)
#ln -fs  ${R_EXPER}/1_namelist          ./1_namelist
#ln -fs  ${R_EXPER}/1_namelist_ice      ./1_namelist_ice
#ln -fs  ${R_EXPER}/AGRIF_FixedGrids.in ./AGRIF_FixedGrids.in

#- XML_IO
ln -fs  ${R_EXPER}/xmlio_server.def ./
ln -fs  ${R_EXPER}/iodef.xml iodef.xml

#- copy configuration files
ln -fs ${R_CONFIG}/ahmcoef                       ./ahmcoef
ln -fs ${R_CONFIG}/bathy_meter_050308_UKMO.nc    ./bathy_meter.nc
ln -fs ${R_CONFIG}/coordinates_ukorca1.nc        ./coordinates.nc
ln -fs ${R_CONFIG}/runoff_1m_ORCA1.nc            ./runoff_1m.nc
ln -fs ${R_CONFIG}/sss_1m_ORCA1.nc               ./sss_1m.nc
ln -fs ${R_CONFIG}/sst_1m_ORCA1.nc               ./sst_1m.nc
ln -fs ${R_CONFIG}/K1rowdrg_R1_modif.nc          ./K1rowdrg.nc
ln -fs ${R_CONFIG}/M2rowdrg_R1_modif.nc          ./M2rowdrg.nc
ln -fs ${R_CONFIG}/mask_itf_ORCA1_new.nc         ./mask_itf.nc 

ln -fs ${R_CONFIGZ}/bathy_level46_050308_UKMO.nc ./bathy_level.nc 
ln -fs ${R_CONFIGZ}/potemp_1m_z46_nomask.nc      ./potemp_1m.nc
ln -fs ${R_CONFIGZ}/salin_1m_z46_nomask.nc       ./salin_1m.nc

#- copy forcing files
ln -fs ${R_FORCING}/u_10.15JUNE2009.nc           ./u_10.nc
ln -fs ${R_FORCING}/v_10.15JUNE2009.nc           ./v_10.nc
ln -fs ${R_FORCING}/q_10.15JUNE2009.nc           ./q_10.nc
ln -fs ${R_FORCING}/t_10.15JUNE2009.nc           ./t_10.nc
ln -fs ${R_FORCING}/ncar_rad.15JUNE2009.nc       ./ncar_rad.nc
ln -fs ${R_FORCING}/ncar_precip.15JUNE2009.nc    ./ncar_precip.nc

ln -fs ${R_FORCINGW}/weights_grid03_bicubic_orca1.nc  ./bic_weights.nc
ln -fs ${R_FORCINGW}/weights_grid03_bilinear_orca1.nc ./bil_weights.nc

#- PBS batch job script
ln -fs ${R_EXPER}/job_pbs job_pbs

#- copy utility tools
ln -fs ${R_EXEDIR}/TOOLS/REBUILD/flio_rbld.exe ./
ln -fs ${R_EXEDIR}/TOOLS/REBUILD/rebuild ./
ln -fs ${R_EXEDIR}/TOOLS/UTIL_HPC/srt_data ./
ln -fs ${R_EXEDIR}/TOOLS/UTIL_HPC/clr_link ./
ln -fs ${R_EXEDIR}/TOOLS/UTIL_HPC/chk_date ./

ls -alF
