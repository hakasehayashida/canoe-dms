#!/bin/bash
#set -x
set -o posix
#set -u
#set -e
#+
#
# ===============
# Fgo_to_tools.sh
# ===============
#
# --------------------------
# Go to the TOOLS directory
# --------------------------
#
# SYNOPSIS
# ========
#
# ::
#
#  $ Fgo_to_tools.sh
#
#
# DESCRIPTION
# ===========
#
#
# Go to the TOOLS directory
#
# EXAMPLES
# ========
#
# ::
#
#  $ ./Fgo_to_tools.sh
#
#
# TODO
# ====
#
# option debug
#
#
# EVOLUTIONS
# ==========
#
# $Id: Fgo_to_TOOLS.sh 2263 2010-10-14 10:13:18Z rblod $
#
#
#
#   * creation
#
#-

cd ${MAIN_DIR}/TOOLS
